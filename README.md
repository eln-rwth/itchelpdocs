# ITC Help Docs

Welcome to the ITC Help Documents for ELN@RWTH. This is where the service management maintains the docs for ITC Help.

## Contributing

Do you think something is missing from the ITC Help Documentation of ELN@RWTH? You can either contact the [IT-ServiceDesk](mailto:servicedesk@itc.rwth-aachen.de), create an [issue](https://git.rwth-aachen.de/eln-rwth/itchelpdocs/-/issues), or make changes to the documents in a new branch or fork and create a merge request so we can review your changes. We will then ensure that these changes are communicated with the ITC Help staff.
