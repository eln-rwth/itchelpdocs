---

title: "General Terms of Use for the Service ELN@RWTH (Pilot Phase)"
author: Nicole Parks, Lukas C. Bossert / IT Center
date: "2024-05-24"
keywords: ELN@RWTH

---

## Preamble

The "ELN@RWTH" service provides a web-based electronic lab notebook system that facilitates the recording and processing of data in compliance with GLP (Good Laboratory Practice) standards for researchers. The service is operated as Software-as-a-Service (SaaS) for RWTH Aachen University and is supported by the IT Center.

## Use of the "ELN@RWTH" Service

All members of RWTH Aachen University are entitled to use the "ELN@RWTH" service to support their research activities with the instance "research.eln.rwth-aachen.de" and in teaching with the instance "education.eln.rwth-aachen.de".

The terms of use listed here must be accepted by the users before using the service during the first login to the respective instance. Once the terms of use are accepted, no further confirmation is required unless the terms of use change. If a user wishes to review the terms of use again, the current terms of use are published in our documentation portal (itc.help).

In case of violation of these terms of use, we reserve the right to block individual projects and accounts.

### Costs

The use of the "ELN@RWTH" service is free of charge.

## Data Storage and Deletion

### Type of Stored Data

The data stored in the electronic lab notebook (ELN) include research data such as measurement data, interview data, documentation, and metadata collected in the context of a research project. This includes certain personal data, such as the name and contact details of a researcher. These are used exclusively for documenting research activities (see "Guidelines for Ensuring Good Scientific Practice," DFG, August 2019).

The processing, storage, or archiving of sensitive research data (see Art. 9 GDPR) in the ELN is expressly prohibited. The use of the ELN must strictly comply with the requirements of Good Scientific Practice.

### Data Deletion

To comply with the documentation obligation according to Good Scientific Practice, user accounts and the entries created within the ELN are retained for at least 10 years.

* Master data: User data can be archived by administrators. This means all experiments of the user are locked, and their login to the system is deactivated. These personal data can also be anonymized upon request by the system administrators. Complete deletion of data within the ELN is not intended.
* Metadata and research data (if personal): To comply with the documentation obligation according to Good Scientific Practice, user accounts and the entries created within the ELN are retained for at least 10 years. They cannot be deleted at the current time.

### Data Protection

The collection, use, and handling of personal data are carried out in compliance with the relevant data protection regulations. In particular, no personal data is disclosed to third parties without authorization. Personal data is used for research purposes and for the traceability of research results as well as for the technical operation of the service.

## Rights and Obligations of the Users

Users are solely responsible for the data they post in the "ELN@RWTH" service, including its quality and accuracy, as well as compliance with third-party rights. Users must also adhere to the above-mentioned [type of stored data](#type-of-stored-data).

### Team Admins

Team admins are a special group of users. They manage their local team, which can be an institute or a research group. There can be multiple admins. At least one admin is the contact person for the central system admins at the IT Center.

### Access Data

Users are required to keep their access data confidential and protect it from third-party access. In the event of possible misuse of access data, the local administrator must be informed immediately. Users are responsible for the consequences of any misuse of their access data.
