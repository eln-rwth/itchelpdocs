# ELN@RWTH

**ELN@RWTH** ist ein zentrales elektronisches Labornotizbuch (ELN) für Mitglieder der RWTH Aachen, angeschlossene Institute und Kooperationspartner.

Der bereitgestellte Service ist die Open-Source-Lösung [eLabFTW](https://www.elabftw.net/), die von der französischen Firma [Deltablot](https://www.deltablot.com/) als Software-as-a-Service gehostet wird.

## Warum eLabFTW

eLabFTW ist ein sehr flexibles, domänenunabhängiges ELN. In den letzten Jahren hat sich eine große, deutschlandweite Community um dieses ELN etabliert, und es ist bekannt für seine solide Support-Infrastruktur und eine transparente, Open-Source-Entwicklungsphilosophie.

Jedoch gibt es für bestimmte Bereiche möglicherweise passendere Optionen.

Wenn Sie unsicher sind, ob eLabFTW für Ihre Forschung geeignet ist, können Sie die folgenden Ressourcen besuchen:

- [eLabFTW Tutorials der Hessischen Forschungsdaten-Infrastrukturen (HeFDI)](https://ilias.uni-marburg.de/goto.php?target=crs_3174359&client_id=UNIMR)
- [eLabFTW Live-Demo](https://demo.elabftw.net/)

Sie können auch eine Beratung über den [IT-ServiceDesk](mailto:servicedesk@itc.rwth-aachen.de) anfragen.

## Umfang von eLabFTW

In erster Linie dient eLabFTW der Dokumentation von Forschung. Dies umfasst die experimentelle Planung, die Durchführung der Experimente, die Schritte zur Verarbeitung und Analyse der Ergebnisse, aber auch die Geräte und Werkzeuge, die in Ihrem Labor verwendet werden.

Neben der grundlegenden Dokumentation ermöglicht Ihnen eLabFTW, Elemente innerhalb des ELN einfach zu verlinken, Einträge zu kopieren, Daten anzuhängen und Ihre Arbeit mit anderen in Ihrem Labor (oder auch darüber hinaus - wenn Sie möchten) zu teilen.

Das fasst seine primären Funktionen zusammen. Was es *nicht* ist, ist eine Software zur Verarbeitung und Analyse Ihrer experimentellen Daten. Dies ist einfach nicht das Ziel dieses spezifischen ELN. Während häufig neue Funktionen hinzugefügt werden und sich die Plattform in Zukunft weiterentwickeln wird, ist es wichtig, dies im Hinterkopf zu behalten.

Was es bietet, sind Methoden zur Schnittstellenanbindung an andere Werkzeuge mithilfe seiner [REST API](https://doc.elabftw.net/api.html). Dies ermöglicht Ihnen die Automatisierung von Workflows zum und vom ELN.

## Erste Schritte

Um mit dem Service zu beginnen, müssen Sie sich als [neues Team registrieren](2_newTeams/21_newTeams.md) und mindestens einen lokalen [Team-Admin](2_newTeams/21_newTeams.md) benennen.

Sobald das Team registriert ist, können sich die Team Admins sowie alle hinzugefügten Nutzenden [anmelden](3_gettingStarted/31_logIn.md) und mit der [Einrichtung ihres ELN](3_gettingStarted/32_setupTeam.md) beginnen.

Diese Dokumentation soll internen Nutzenden den Einstieg in den Service ELN@RWTH erleichtern. Ausführlichere Dokumentationen zur Nutzung finden Sie in der [eLabFTW-Dokumentation](https://doc.elabftw.net/).

## Further Information

[Terms of Service](Nutzungsbedingungen-ELNRWTH.md)

[Privacy Policy](Datenschutzhinweise-ELNRWTH.md)