# ELN@RWTH

**ELN@RWTH** is a central electronic lab notebok (ELN) for RWTH Aachen members, affiliated institutes, and collaboration partners.

The service provided is the open-source solution [eLabFTW](https://www.elabftw.net/), hosted as a software as a service by the French company [Deltablot](https://www.deltablot.com/).

## Why eLabFTW

eLabFTW is a very flexible, domain-agnostic ELN. In the last few years, a large Germany-wide community has established itself around this ELN and it is known to have a sound support infrastructure and a transparent, open-source development philosophy.

That being said, for certain domains, there may be more fitting options.

If you are unsure whether eLabFTW is right for your research, you can visit the following resources:

- [eLabFTW Tutorials by the Hessian Research Data Infrastructures (HeFDI)](https://ilias.uni-marburg.de/goto.php?target=crs_3174359&client_id=UNIMR)
- [elabFTW Live Demo](https://demo.elabftw.net/)

You can also request a consultation session via the [IT-ServiceDesk](mailto:servicedesk@itc.rwth-aachen.de).

## Scope of eLabFTW

First and foremost, eLabFTW serves to document research. This includes experimental planning, carrying out the experiments, the steps in processing and analyzing results, but also the devices and tools used by your lab.

In addition to basic documentation, eLabFTW allows you to easily link items within the ELN, copy entries, attach data and share your work with others in your lab (or even beyond - if you want to).

That sums up its primary functions. What it is *not* is software to process and analyze your experimental data. This is simply not the aim of this specific ELN. While new features are added often and the platform may evolve in the future, it is important to keep this in mind.

What it does offer are methods of interfacing with other tools using its [REST API](https://doc.elabftw.net/api.html). This allows you to automate workflows to and from the ELN.

## Getting Started

To get started with the service, you will need to [register as a new team](2_newTeams/21_requestNewTeam.md) and choose at least one local [Team Admin](2_newTeams/22_teamAdmin.md).

Once the team has been registered, the team admin as well as any added users can [login](3_gettingStarted/31_logIn.md) and start [setting up their ELN](3_gettingStarted/32_setupTeam.md).

This documentation aims to get internal users started with the service ELN@RWTH. More in-depth documentation on usage can be found in the [eLabFTW documentation](https://doc.elabftw.net/).

## Further Information

[Terms of Service](Nutzungsbedingungen-ELNRWTH_en.md)

[Privacy Policy](Datenschutzhinweise-ELNRWTH_en.md)