---

title: "Information on the Collection and Processing of Personal Data for the Service ELN@RWTH (Pilot Phase)"
author: Nicole Parks, Lukas C. Bossert / IT Center
date: "2024-05-24"
keywords: ELN@RWTH

---

Information on the collection and processing of personal data for the documentation and management of research data and the use of an ELN as part of the *ELN@RWTH* service.

## Service
Collection and processing of personal data for the documentation and management of research data and the use of an ELN as part of the ELN@RWTH service.

## Description of the Service
The _ELN@RWTH_ service uses the product eLabFTW provided by Deltablot (elabftw.net), a web-based electronic lab notebook system that facilitates GLP (Good Laboratory Practice) compliant recording of data by researchers. The system is operated by Deltablot as Software-as-a-Service (SaaS) for RWTH Aachen University and supported by the IT Center.

## Name and Address of the Data Controller
The controller in the sense of the General Data Protection Regulation and other national data protection laws of the member states as well as other data protection regulations is:

Rector of RWTH Aachen University  
Templergraben 55  
52062 Aachen (Street address)  
52056 Aachen (Mailing address)  
Phone: +49 241 80 1  
Fax: +49 241 80 92312  
Email: rektorat@rwth-aachen.de  
Website: www.rwth-aachen.de/rektorat  

For the implementation of the _ELN@RWTH_ service:

IT Center of RWTH Aachen  
Seffenter Weg 23  
52074 Aachen  
Phone: +49 241 80 24680  
Email: servicedesk@itc.rwth-aachen.de   
Website: www.itc.rwth-aachen.de

## Subject and Purpose of Processing Personal Data
The data listed below are collected for the purpose of documenting research activities and research data (see "Guidelines for Ensuring Good Scientific Practice," DFG, August 2019) and managing access rights to the _ELN@RWTH_ service.

## Type of Personal Data of the Data Subjects / Special Categories of Personal Data
For the _ELN@RWTH_ service, the following personal data are processed and stored:

* Master data: First name, last name, email address, ORCiD ID for account creation
* Metadata: IP address, timestamp when entries are modified
* Research data/content data: Name, first name in the context of references to collaboration partners; depending on the research project (e.g., interview data)

## Recipients of the Data
* Deltablot  
116 AV de Paris  
94800 Villejuif  
France  
Registration number: 922 205 109 R.C.S. Créteil  
Email: contact@deltablot.email  
Website: deltablot.com  

* The IT Center of RWTH Aachen
* The respective RWTH department
* External users in the form of collaboration partners

## Legal Basis
The aforementioned master data (including name, email address) are collected in accordance with Art. 6 para. 1 sentence 1 lit. e), para. 3 GDPR in conjunction with § 18 para. 1 DSG NRW (login data collected directly). Metadata (including log files, timestamps, IP address) are collected in accordance with Art. 6 para. 1 sentence 1 lit. e), para. 3 in conjunction with § 3 para. 1 HG NRW, § 58 para. 1 DSG NRW. Content/research data (if personal) are collected in accordance with Art. 6 para. 1 sentence 1 lit. a) GDPR with consent or Art. 6 para. 1 sentence 1 lit. e), para. 3 GDPR in conjunction with § 17 para. 1 DSG NRW when the research privilege applies.

**Note:** The pilot phase of the “ELN@RWTH” service is conducted with a selected group of pilot customers from the research sector. The use of education.eln.rwth-aachen.de (for students) is not supported during the pilot phase. This is also because the legal basis for students (and guest researchers) has not yet been fully clarified.

## Data Deletion and Retention Period
To comply with the documentation obligation according to Good Scientific Practice, user accounts and the entries created within the service are retained for at least 10 years.

* Master data: User data can be archived by administrators. This means all experiments of the user are locked, and their login to the system is deactivated. These personal data can also be anonymized upon request by the system administrators. Complete deletion of data within the ELN is not intended.
* Metadata and research data (if personal): To comply with the documentation obligation according to Good Scientific Practice, user accounts and the entries created within the ELN are retained for at least 10 years. They cannot be deleted at the current time.

## Rights of the Data Subject
If personal data is processed by the undersigned, they are a data subject in the sense of the GDPR and have the following rights vis-à-vis the controller.

According to Articles 15 ff. GDPR, they have the right to access the personal data concerning them, as well as the right to rectification or deletion, restriction of processing, objection to processing, and the right to data portability, under the conditions defined there. According to Article 77 GDPR, they also have the right to lodge a complaint with the data protection supervisory authority (https://www.ldi.nrw.de/), if the data subject believes that the processing of their personal data violates this regulation. If the processing is based on the data subject's consent (see Art. 6 para. 1 lit. a, Art. 9 para. 2 lit. a GDPR), they also have the right to withdraw the consent at any time, without affecting the lawfulness of the processing based on the consent before its withdrawal; this takes effect only for the future.

To exercise the aforementioned rights, the undersigned should send an email to servicedesk@itc.rwth-aachen.de.