# FAQ

Q: How long is the pilot phase planned for and what happens afterwards?\
A: The pilot phase is planned for 1 year. If successful, the instance used for this phase will be continued requiring no action from the pilot users.

In the event of termination of the Service Agreement, the IT Center is to receive all data within 5 days in a format that is reusable by the eLabFTW software. Thus, an new instance could be made available.

Q: Is there a planned end date for the service as a whole?\
A: No, the service is planned indefinitely.

Q: How is the data backed up?\
A: The main backup is taken care of by the service provider, [Deltablot](https://www.deltablot.com/). This includes daily, geo-redundant backups. Daily backups are kept for 7 days, weekly backups for 4 weeks, and monthly backups for 4 weeks.

The IT Center reserves the right to read access these backups.

In addition, the data is also backed up locally by the IT Center.

In the event of termination of the Service Agreement, the IT Center is to receive all data within 5 days in a format that is reusable by the eLabFTW software.

Q: How can I access the ELN?\
A: Once you you have been granted access either by the IT Center System Admins or your Team Admin, you can access the ELN through the Shibboleth single-sign-son (SSO). A VPN is not necessary.

Q: I have been added as a User by my Team Admin, but receive an error after logging in.
A: This may be due to an e-mail-mismatch. When you log in, verify that the e-mail address in the data being submitted to the system matches the e-mail address used when the user was set up in the ELN. If the e-mail address is not an RWTH/institutional address, please adjust your contact email in the [IdM Selfservice under Personal Data](https://idm.rwth-aachen.de/selfservice/PersonalData?1).

Q: How much data can be stored in eLabFTW?\
A: The instance has 500 GB of server space. Please do not attach large data files to the experiments. For such cases, you can use [Coscine](https://coscine.rwth-aachen.de/) and link your resources and files to your experiments.

Q: How is eLabFTW structured?\
A: eLabFTW is split into teams. Teams, in this case, should represent individual research groups or instutes, or even large projects such as Collaborative Research Centers (CRCs). Team Admins can further sub-devide these teams into groups. During the pilot phase, we will gather feedback from the pilot users go determine what level of granularity works best for the team structure and whether it makes sense to assign multiple teams to one research group or institute.

Q: Can I edit the *Extra Fields* that I have already added to templates and experiment?\
A: Yes, you can edit these fields in the JSON editor. If you are correcting spelling errors or similar, the form view will suffice. If you need to correct more details it is easier to do so using the code view.

Q: Can I create a template from an experiment?\
A: Not directly, but there is a workaround. Export your experiment in as an ELN Archive and import it when creating the new template. To bel able to reuse step lists, these need to be unchecked before exporting. You can copy an existing experiment and edit this one to fit the template first.

Q: Is it possible to export data in a standardized manner?\
A: Yes, the ELN format was developed for this purpose. Once unzipped, you are able to retrieve your data. It is also intended to be imported by eLabFTW and other ELNs. See [the ELN Consortium](https://github.com/TheELNConsortium/) for more information.

Q: What is the max. team size during the pilot phase?\
A: The team size is not limited. Depending on the size or organization of individual research groups or institutes, we may recommend creating multiple teams.

Q: Should team admins be IT admins or someone actively involved in the team's research?\
A: There should be at least one team admin who has deeper knowledge of the research, while IT admins may also be team admins. Multiple admins are possible. Keep in mind that team admins take on a organizational role, rather than an IT role since the system is maintained and administered in a central manner.