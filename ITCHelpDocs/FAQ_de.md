# FAQ

F: Wie lange ist die Pilotphase geplant und was passiert danach?\
A: Die Pilotphase ist für 1 Jahr geplant. Bei erfolgreicher Durchführung wird die für diese Phase verwendete Instanz fortgesetzt, ohne dass die Pilotbenutzer*innen und aktiv werden müssen.

Im Falle der Kündigung des Servicevertrags ist das IT Center verpflichtet, alle Daten innerhalb von 5 Tagen in einem Format zu erhalten, das von der eLabFTW-Software wiederverwendbar ist. Somit könnte eine neue Instanz zur Verfügung gestellt werden.

F: Gibt es ein geplantes Enddatum für den Service insgesamt?\
A: Nein, der Service ist unbefristet geplant.

F: Wie erfolgt die Datensicherung?\
A: Die Hauptdatensicherung wird vom Dienstleister, [Deltablot](https://www.deltablot.com/), durchgeführt. Dies umfasst tägliche, geo-redundante Backups. Tägliche Backups werden für 7 Tage aufbewahrt, wöchentliche Backups für 4 Wochen und monatliche Backups ebenfalls für 4 Wochen.

Das IT Center behält sich das Recht vor, auf diese Backups zuzugreifen.

Darüber hinaus werden die Daten auch lokal vom IT Center gesichert.

Im Falle der Kündigung des Servicevertrags ist das IT Center verpflichtet, alle Daten innerhalb von 5 Tagen in einem Format zu erhalten, das von der eLabFTW-Software wiederverwendbar ist.

F: Wie kann ich auf das ELN zugreifen?\
A: Sobald Ihnen der Zugriff entweder von den SystemAdmins des IT Centers oder Ihrem Team Admin gewährt wurde, können Sie auf die Instanz über die Shibboleth-Single-Sign-On (SSO) zugreifen. Ein VPN ist nicht erforderlich.

F: Ich wurde von meinem Team-Admin als User hinzugefügt, erhalte jedoch nach dem Einloggen einen Fehler.
A: Dies könnte an einer E-Mail-Unstimmigkeit liegen. Wenn Sie sich einloggen, überprüfen Sie, ob die E-Mail-Adresse in den übermittelten Daten mit der E-Mail-Adresse übereinstimmt, die bei der Einrichtung des Benutzers im ELN verwendet wurde. Wenn die E-Mail-Adresse keine RWTH/institutionelle Adresse ist, passen Sie bitte Ihre Kontakt-E-Mail im [IdM-Selfservice unter Persönliche Daten](https://idm.rwth-aachen.de/selfservice/PersonalData?1) an.

F: Wie viele Daten können in eLabFTW gespeichert werden?\
A: Die Instanz verfügt über 500 GB Serverplatz. Bitte hängen Sie keine großen Datenfiles an die Experimente an. Für solche Fälle können Sie [Coscine](https://coscine.rwth-aachen.de/) verwenden und Ihre Ressourcen und Dateien mit Ihren Experimenten verknüpfen.

F: Wie ist eLabFTW strukturiert?\
A: eLabFTW ist in Teams unterteilt. Teams sollten in diesem Fall einzelne Forschungsgruppen oder Institute repräsentieren oder sogar große Projekte wie Sonderforschungsbereiche (SFBs). Team Admins können diese Teams weiter in Gruppen unterteilen. Während der Pilotphase sammeln wir Feedback von den Pilotbenutzern, um festzustellen, auf welcher Granularitätsebene die Teamstruktur am besten funktioniert und ob es sinnvoll ist, mehrere Teams einer Forschungsgruppe oder einem Institut zuzuweisen.

F: Kann ich die *Zusatzfelder*, die ich bereits zu Vorlagen und Experimenten hinzugefügt habe, bearbeiten?\
A: Ja, Sie können diese Felder im JSON-Editor bearbeiten. Wenn Sie Rechtschreibfehler oder Ähnliches korrigieren, genügt die Formansicht. Wenn Sie weitere Details korrigieren müssen, ist es einfacher, dies über die Codeansicht zu tun.

F: Kann ich aus einem Experiment eine Vorlage erstellen?\
A: Nicht direkt, aber es gibt einen Workaround. Exportieren Sie Ihr Experiment als ELN-Archiv und importieren Sie es beim Erstellen der neuen Vorlage. Um Schrittlisten wiederverwenden zu können, müssen diese vor dem Exportieren abgewählt werden. Sie können ein vorhandenes Experiment kopieren und dieses zuerst bearbeiten, um es an die Vorlage anzupassen.

F: Ist es möglich, Daten auf standardisierte Weise zu exportieren?\
A: Ja, das ELN-Format wurde für diesen Zweck entwickelt. Sobald es entpackt ist, können Sie Ihre Daten abrufen. Es ist auch beabsichtigt, von eLabFTW und anderen ELNs importiert zu werden. Siehe [das ELN-Konsortium](https://github.com/TheELNConsortium/) für weitere Informationen.

F: Welche Teamgröße ist während der Pilotphase möglich?\
A: Die Teamgröße ist nicht begrenzt. Abhängig von der Größe oder Organisation einzelner Forschungsgruppen oder Institute, empfehlen wir ggf. mehrere Teams zu erstellen.

F: Sollten Team-Admins IT-Admins sein oder eher eine Person, die aktiv in die Forschung des Teams involviert ist?\
A: Es sollte mindestens einen Team-Admin geben, der oder die tiefere Kenntnisse über die Forschung hat, während auch IT-Admins Team-Admins sein können. Mehrere Admins sind möglich. Beachten Sie, dass Team-Admins eher eine organisatorische Rolle übernehmen als eine IT-Rolle, da das System zentral gewartet und verwaltet wird.