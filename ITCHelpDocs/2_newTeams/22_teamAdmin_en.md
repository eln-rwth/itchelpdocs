# Team Admin Guidelines

## Description

Team Admins organize the ELN for their respective team. At least one Team Admin is required for each team. Team Admins also serve as the main contact to the central team for the ELN at the RWTH IT Center. At least one Team Admin should be someone who holds a long-term position within the research group, enabling continuity from one generation of research assistants to the next. While Team Admins do not need to be researchers, having at least one may prove helpful in reflecting the research processes and documentation within the ELN.

The number of persons designated this role should be kept to a minimum.

## Responsibilities

A Team Admin is tasked with the central organization of the teams' ELN. As such they (non-exhaustive list):

- manage users (create new users, designate tra admins, ...)
- setup groups within team (where desired)
- manage entity categories
- are responsible for basic settings
- control deletion of experiments
- make page announcements
- link internal documentation
- edit the standard experiment template
- set whether or not to enforce [read/write permissions](#permission-settings)
- can export experiments, calenders, and resources

Most of these options are found in the Admin panel in the User menu (upper right-hand side of the screen), as shown below:

![finding the admin user menu](Screenshot_userMenu_Admin_en.png)

Moreover, Team Admins should work with the research group to create templates for resources and experiments. While all users can create experiment templates, team admins should ensure these [follow specific guidelines for their group](35_teamPoliciesStandards).

More information for Team Admins can be found on [eLabFTW's Admin Guide](https://doc.elabftw.net/admin-guide.html).

## Permission Settings

By default, eLabFTW has very flexible visibility and write settings for each experiment. For more details, see [Rights and Permissions](../3_gettingStarted//33_rightsPermissions.md).

It is important to set your own team policies for visibility. Team Admins can enforce read/write settings. Keep in mind that:

- Neither the Team Admin nor SysAdmin can override `only owner` settings
- Ownership *can* be transferred
  - But only by owner if `only owner` permission has been set

Note: The `only owner` option is disabled in the ELN@RWTH instances. Since this has to be disabled for the entire instance, please contact us and/or let us know in your onboarding session if you require it.

## Managing Users

A main responsibility of Team Admins will be creating user accounts. This works in a few different ways:

1. Create users prior to sign-in:
   In the Admin panel, as shown below, select the **Users** tab and enter the person's name and e-mail address. Please note: ths address must match the address they use for SSO.

![creating a new user in elabftw](Screenshot_AdminPanel_createUser_en.png)

2. Supply the System Admins a list of users when requesting a new team (Work in Progress!)
   We can add the users when we setup your new team. To do so, please download and submit [this list](./newUsers.xlsx) wit your team request.

## Note on Employee Tracking

As stated in the [Terms of Use](../Datenschutzhinweise-ELNRWTH_en.md), the ELN is explicitly not intended for monitoring or controlling employee performance or similar purposes and must not be misused for this purpose.