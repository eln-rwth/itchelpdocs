# Requesting a New Team in eLabFTW

*Please note: During the pilot phase the amount of teams are limited and evaluated on a case-by-case basis.*

To request a new [team](/ITCHelpDocs/3_gettingStarted/34_terminology.md) in eLabFTW, please contact the [IT-ServiceDesk](mailto:servicedesk@itc.rwth-aachen.de).

You will then be invited to a onboarding session. For this meeting, you will need to provide a completed [team request form](antragTeam_ELNRWTH_en.pdf) or you can submit it with your request to the IT-ServiceDesk. You will also need to select a Team Admin. Please review the [documentation](/ITCHelpDocs/2_newTeams/22_teamAdmin_en.md) for this role.

You are also required to fill out and keep a Verzeichnis der Verarbeitungstätigkeit Verantwortlicher (gem. Artikel 30 Abs. 1 DSGVO) (VVT; English: Record of Processing Activities for Data Controllers (pursuant to Article 30(1) GDPR)) for your internal records. You will receive a template as part of the onboarding process.
