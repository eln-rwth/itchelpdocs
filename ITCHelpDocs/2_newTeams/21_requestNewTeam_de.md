# Beantragung eines neuen Teams in eLabFTW

*Bitte beachten: Während der Pilotphase ist die Anzahl der Teams begrenzt und wird von Fall zu Fall evaluiert.*

Um ein neues [Team](/ITCHelpDocs/3_gettingStarted/34_terminology_de.md) in eLabFTW zu beantragen, kontaktieren Sie bitte den [IT-ServiceDesk](mailto:servicedesk@itc.rwth-aachen.de).

Sie werden dann zu einer Onboarding-Session eingeladen. Für dieses Treffen müssen Sie ein ausgefülltes [Team-Antragsformular](antragTeam_ELNRWTH_de.pdf) mitbringen bzw. können Sie dies ihrer Anfrage beim IT-ServiceDesk beifügen. Dazu müssen Sie einen Team-Administrator auswählen. Bitte lesen Sie die [Dokumentation](/ITCHelpDocs/2_newTeams/22_teamAdmin_de.md) für diese Rolle.

Sie müssen außerdem ein Verzeichnis der Verarbeitungstätigkeiten Verantwortlicher (gem. Artikel 30 Abs. 1 DSGVO) (VVT) ausfüllen und für Ihre internen Aufzeichnungen aufbewahren. Sie erhalten eine Vorlage im Rahmen des Onboarding-Prozesses.
