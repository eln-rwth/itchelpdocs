# Richtlinien für Team-Admins

## Beschreibung

Team-Administratoren organisieren das ELN für ihr jeweiliges Team. Für jedes Team ist mindestens ein Team-Administrator erforderlich. Team-Administratoren fungieren auch als Hauptkontakt zum zentralen Team für das ELN am IT Center der RWTH. Mindestens ein Team-Administrator sollte eine langfristige Position innerhalb der Forschungsgruppe innehaben, um Kontinuität von einer Generation von Forschungsassistenten zur nächsten zu gewährleisten. Obwohl Team-Administratoren keine Forscher sein müssen, kann es hilfreich sein, mindestens einen Forscher im Team zu haben, um die Forschungsprozesse und die Dokumentation im ELN widerzuspiegeln.

Die Anzahl der Personen, die diese Rolle übernehmen, sollte auf ein Minimum beschränkt werden.

## Verantwortlichkeiten

Ein Team-Administrator ist für die zentrale Organisation des ELNs seines Teams zuständig. Dazu gehören unter anderem folgende Aufgaben:

- Benutzer verwalten (neue Benutzer erstellen, Team-Admins zuweisen, ...)
- Gruppen innerhalb des Teams einrichten (wo gewünscht)
- Kategorien für Entities verwalten
- Verantwortlich für Grundeinstellungen
- Kontrolle über die Löschung von Experimenten
- Seitenankündigungen machen
- Interne Dokumentation verlinken
- Standard-Experimentvorlage bearbeiten
- Festlegen, ob Lese-/Schreibrechte [erzwingen](#berechtigungseinstellungen) werden sollen
- Können Experimente, Kalender und Ressourcen exportieren

Die meisten dieser Optionen befinden sich im Admin-Panel im Benutzermenü (oben rechts auf dem Bildschirm), wie unten gezeigt:

![finding the admin user menu](Screenshot_userMenu_Admin_de.png)

Darüber hinaus sollten Team-Administratoren mit der Forschungsgruppe zusammenarbeiten, um Vorlagen für Ressourcen und Experimente zu erstellen. Obwohl alle Benutzer Experimentvorlagen erstellen können, sollten Team-Administratoren sicherstellen, dass diese [den spezifischen Richtlinien ihrer Gruppe folgen](35_teamPoliciesStandards_de.md).

Weitere Informationen für Team-Administratoren finden Sie im [Admin-Guide von eLabFTW](https://doc.elabftw.net/admin-guide.html).

## Berechtigungseinstellungen

Standardmäßig bietet eLabFTW sehr flexible Sichtbarkeits- und Schreibrechte für jedes Experiment. Für mehr Details siehe [Rechte und Berechtigungen](../3_gettingStarted/33_rightsPermissions_de.md).

Es ist wichtig, eigene Teamrichtlinien für die Sichtbarkeit festzulegen. Team-Administratoren können Lese-/Schreibrechte erzwingen. Beachten Sie:

- Weder der Team-Admin noch der SysAdmin können `nur Eigentümer`-Einstellungen überschreiben
- Eigentum *kann* übertragen werden
  - Aber nur durch den Eigentümer, wenn `nur Eigentümer`-Berechtigung gesetzt wurde

Hinweis: Die Option `nur Eigentümer` ist in der ELN@RWTH-Instanz deaktiviert. Da dies für die **gesamte** Instanz deaktiviert werden muss, kontaktieren Sie uns bitte und/oder informieren Sie uns in Ihrer Einweisung, wenn Sie dies benötigen.

## Benutzerverwaltung

Eine Hauptverantwortung der Team-Administratoren besteht darin, Benutzerkonten zu erstellen. Dies funktioniert auf verschiedene Weise:

1. Benutzer vor dem Anmelden erstellen:
   Im Admin-Panel, wie unten gezeigt, wählen Sie die Registerkarte **Benutzer** und geben den Namen und die E-Mail-Adresse der Person ein. Bitte beachten Sie: Diese Adresse muss mit der Adresse übereinstimmen, die sie für SSO verwenden.

![creating a new user in elabftw](Screenshot_AdminPanel_createUser_de.png)

2. Übermitteln Sie den Systemadministratoren eine Liste von Benutzern, wenn Sie ein neues Team anfordern (in Arbeit!)
   Wir können die Benutzer hinzufügen, wenn wir Ihr neues Team einrichten. Laden Sie dazu bitte [diese Liste](./newUsers.xlsx) herunter und übermitteln Sie sie mit Ihrer Team-Anfrage.

## Hinweis zur Mitarbeiterüberwachung

Wie in den [Nutzungsbedingungen](../Datenschutzhinweise-ELNRWTH_en.md) angegeben, ist das ELN ausdrücklich nicht zur Überwachung oder Kontrolle der Mitarbeiterleistung oder ähnlichen Zwecken vorgesehen und darf nicht zu diesem Zweck missbraucht werden.