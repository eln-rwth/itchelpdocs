# Community and Resources

## Chat

The central ELN@RWTH has setup a central Matrix chat room for easy communication.
This is intended for general topics regarding ELNs and is *not* limited to eLabFTW or the RWTH's central instance.
It is currently not a public chatroom, which may change in the future.
For access, send an email to the [IT-ServiceDesk](mailto:servicedesk@itc.rwth-aachen.de).

There is also a central [chat room for eLabFTW](https://app.gitter.im/#/room/#elabftw_elabftw:gitter.im) for quick assistance from the developers and the international eLabFTW community.

## GitLab Group

The [ELN-RWTH GitLab Group](https://git.rwth-aachen.de/eln-rwth) serves as a central place to collect material and information regarding ELNs. This is an ongoing community effort—feel free to contribute!
