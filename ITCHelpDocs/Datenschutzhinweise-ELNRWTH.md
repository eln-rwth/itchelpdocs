---
title: "Information zur Erhebung und Verarbeitung personenbezogener Daten im Zuge für den Dienst ELN@RWTH (Pilotphase)"
author: Nicole Parks, Lukas C. Bossert / IT Center
date: "2024-05-24"
keywords: ELN@RWTH
---
# Information zur Erhebung und Verarbeitung personenbezogener Daten im Zuge für den Dienst ELN@RWTH (Pilotphase)

Information zur Erhebung und Verarbeitung personenbezogener Daten zur
Dokumentation und Verwaltung von Forschungsdaten und Nutzung eines ELNs im
Rahmen des Dienstes „ELN@RWTH“.

## Dienst
Erhebung und Verarbeitung personenbezogener Daten zur Dokumentation und
Verwaltung von Forschungsdaten und Nutzung eines ELNs im Rahmen des Dienstes
ELN@RWTH.

## Beschreibung des Dienstes
Als Dienst „ELN@RWTH“ dient das Produkt eLabFTW der Firma Deltablot elabftw.net,
ein webbasiertes elektronisches Labornotizbuch-System, das Forschenden die
Aufzeichnung von Daten in GLP (Good Laboratory Practice)-konformer Weise
erleichtert. Das System wird von der Firma Deltablot als Software-as-a-Service
(SaaS) für die RWTH Aachen University betrieben und durch das IT Center
unterstützt.

## Name und Anschrift des Verantwortlichen
Der Verantwortliche im Sinne der Datenschutz-Grundverordnung und anderer
nationaler Datenschutzgesetze der Mitgliedsstaaten sowie sonstiger
datenschutzrechtlicher Bestimmungen ist der:

Rektor der RWTH Aachen University  
Templergraben 55  
52062 Aachen (Hausanschrift)   
52056 Aachen (Postanschrift) 
Telefon: +49 241 80 1  
Telefax: +49 241 80 92312  
E-Mail: rektorat@rwth-aachen.de   
Website: www.rwth-aachen.de/rektorat  

Für die Umsetzung des Dienstes „ELN@RWTH“:

IT Center der RWTH Aachen  
Seffenter Weg 23  
52074 Aachen  
Telefon: +49 241 80 24680  
E-Mail: servicedesk@itc.rwth-aachen.nde   
Webseite: www.itc.rwth-aachen.de

## Gegenstand und Zweck der Verarbeitung personenbezogener Daten
Die unten genannten Daten werden zum Zwecke der Dokumentation von Forschungstätigkeiten und Forschungsdaten erhoben (vgl. „Leitlinien zur Sicherstellung guter wissenschaftlicher Praxis“, DFG, August 2019) sowie zur Verwaltung der Zugriffsrechte auf den Dienst „ELN@RWTH“.

## Art der personenbezogenen Daten der Betroffenen / besondere Kategorien von personenbezogenen Daten
Für den Dienst „ELN@RWTH“ erfolgt die Verarbeitung und Speicherung folgender
personenbezogener Daten:

* Stammdaten: Vorname, Nachname, E-Mail-Adresse, ORCiD-ID zur Kontoerstellung
* Metadaten: IP-Adresse, Zeitstempel bei Eintragsänderung
* Forschungsdaten/Inhaltsdaten: Name, Vorname im Sinne des Verweises auf Kollaborationspartner; abhängig vom Forschungsprojekt (z. B. Interviewdaten)

## Empfänger der Daten
* Deltablot  
116 AV de Paris  
94800 Villejuif  
Frankreich  
Immatrikulationsnr.: 922 205 109 R.C.S. Créteil   
e-Mail: contact@deltablot.email  
Webseite: deltablot.com  

* Das IT Center der RWTH Aachen
* Die jeweilige RWTH-Einrichtung
* Externe Nutzende in Form von Kollaborationspartnern

## Rechtsgrundlage

Die oben genannten Stammdaten (unter anderem Name, E-Mail-Adresse) werden gemäß
Art. 6 Abs. 1 S. 1 lit. e), Abs. 3 DSGVO i.V.m. § 18 Abs. 1 DSG NRW erhoben
(Login-Daten direkt erhoben). Metadaten werden (unter anderem Logfiles, Zeitstempel, IP-Adresse) gemäß Art. 6 Abs. 1 S. 1 lit. e), Abs. 3 i.V.m. § 3 Abs. 1 HG NRW, § 58 Abs. 1 DSG NRW erhoben. Inhaltsdaten/ Forschungsdaten (wenn personenbezogen) werden gemäß Art. 6 Abs. 1 S. 1 lit. a) DSGVO bei Einwilligung oder Art. 6 Abs. 1 S. 1 lit. e), Abs. 3 DSGVO i.V.m. § 17 Abs. 1 DSG NRW bei Einschlägigkeit des Forschungsprivilegs erhoben.

**Hinweis:** Die Pilotphase des Dienstes “ELN@RWTH” findet mit einem ausgewählten Kreis an Pilotkunden aus dem Forschungsbereich statt. Die Nutzung von education.eln.rwth-aachen.de (für Studierende) wird in der Pilotphase nicht unterstützt. Nicht zuletzt auch daher, da die Rechtsgrundlage für Studierende (und Gastwissenschaftler) noch nicht abschließend geklärt ist.


## Datenlöschung und Speicherdauer
Für die Einhaltung der Dokumentationspflicht gemäß der Guten Wissenschaftlichen
Praxis werden Nutzendenkonten und damit die erstellten Einträge innerhalb des
Dienstes für mindestens 10 Jahre vorgehalten. 

* Stammdaten: Nutzerdaten können von Administratoren archiviert werden. Somit sind alle Experimente des Nutzenden gesperrt sowie deren Login zum System deaktiviert. Zudem können diese personenbezogenen Daten auf Anfrage über die SysAdmins anonymisiert werden. Das vollständige Löschen von Daten ist innerhalb des ELN nicht vorgesehen.
* Metadaten und Forschungsdaten (wenn personenbezogen): Für die Einhaltung der
  Dokumentationspflicht gemäß der Guten Wissenschaftlichen Praxis werden Nutzendenkonten und damit die erstellten Einträge innerhalb des ELN für mindestens 10 Jahre vorgehalten. Sie können nach aktuellem Stand nicht gelöscht werden.

## Rechte der betroffenen Person
Werden personenbezogene Daten von dem Unterzeichnenden verarbeitet, ist er
Betroffener i.S.d. DS-GVO und es stehen ihm folgende Rechte gegenüber dem
Verantwortlichen zu.

Er hat gemäß Artikel 15 ff. DS-GVO dem Verantwortlichen gegenüber unter den dort definierten Vor- aussetzungen das Recht auf Auskunft über die betreffenden personenbezogenen Daten sowie auf Berichtigung oder Löschung oder auf Einschränkung der Verarbeitung, ein Widerspruchsrecht gegen die Verarbeitung sowie das Recht auf Datenübertragbarkeit. Auch hat er gemäß Artikel 77 DS-GVO das Recht der Beschwerde bei der Datenschutz-Aufsichtsbehörde (https://www.ldi.nrw.de/), wenn der Betroffene der Ansicht ist, dass die Verarbeitung der ihn betreffenden personenbezogenen Daten gegen diese Verordnung verstößt. Wenn die Verarbeitung auf einer Einwilligung des Betroffenen beruht (vgl. Art. 6 Abs. 1 lit. a, Art. 9 Abs. 2 lit. a DSGVO), hat er ferner das Recht, die Einwilligung jederzeit zu widerrufen, ohne dass die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung berührt wird; dieser entfaltet seine Wirkung erst für die Zukunft.

Zur Ausübung der vorgenannten Rechte sendet der Unterzeichner eine E-Mail an servicedesk@itc.rwth-aachen.de.
