# Signing In to the Service

To log in to the [RWTH's research instance of eLabFTW](https://research.eln.rwth-aachen.de/), you will need to select your institution from the dropdown list. 

![eLabFTW login page](Screenshot_Login_eLabFTW_de.png.png)

You will then be forwarded to your local Shibboleth single-sign-on (SSO) page. Entering your credentials signs you in to the ELN.

If you see the error message pictured below, contact either your Team Admin to create your user account or [request a new Team](/ITCHelpDocs/2_newTeams/21_requestNewTeam_en.md) via the [IT-ServiceDesk](mailto:mailto:servicedesk@itc.rwth-aachen.de).

![No user error upon signing in to eLabFTW.](signin_error.png)