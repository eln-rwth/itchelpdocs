# Team Policies, Standards, and Guidelines

This article does not concern the software itself and is aimed at ensuring your institute or research group effectively transitions to an ELN.

As opposed to a paper notebook, ELNs have the power to digitally represent your lab as opposed to one person's research. Therefore, it is important to set usage standards, policies, and guidelines within your group. It is recommended to assign a designated taskforce to test the service, implement it within the group, and maintain and update how the group works with the tool.

This taskforce (or at least one member of it) also serves as the main contact to the central support at the IT Center and can thus communicate desired features, problems, and so on. At least one person in this group also must be a [Team Admin](/ITCHelpDocs/2_newTeams/22_teamAdmin_en.md) so they can implement standards, restrictions, and other measures in the tool itself.

## Templates

Resource and experiment templates can greatly alleviate data documentation and also ensure all group members document their work in a similar manner. 

Team Admins can enforce the use of templates. Agree on standards on what information needs to be collected. This may be a group effort, or an individual one, but those working with similar methods should consult one another to ensure their documentation is consistent to a certain degree. Furthermore, resources, such as devices will all contain similar information such as manufacturer, country, make, and model. Including all of this information in a template ensures no information is accidentally omitted when, for example, a new device is acquired.

## Digitize the Lab

Start collecting all of your lab research data workflows. This models how information flows in your lab and how things are linked to one another. 

![example of mindmap to model lab processes](2023-09-16_Data-Map_Example-1.png)

Example of modelling research activities and workflows. Image attribution: Grünwald, K.E., from Poster: *From a data inventory to a data management plan (DMP)*,  DOI [10.18154](https://doi.org/10.18154/RWTH-2023-10436).

It serves two purposes in the context of setting up and ELN:

1. Identify what needs to be recorded in the ELN (e.g., devices, software...)
2. Identify which workflows can be improved or automated (e.g., automatically attaching raw data files to experiments)

## Onboarding and Offboarding Group Members

New group members need to know how they are expected to use the ELN. Which read write permission should experiments have? Who is allowed to see what? What data needs to be attached to experiments?

Perhaps more importantly, decide how experiments will be handed over to group leads at the end of employment. This could involve transferring ownership, exporting all experiments to an archive solution, or simply using the archive function within the notebook. **It is important to ensure that supervisors have full access to completed experiments to be able to access, view, and reuse the information contained.**

## Further Resources

[ZB MED ELN-Wegweiser](https://www.zbmed.de/fileadmin/user_upload/PUBLISSO/PUBLISSO_ELN-Wegweiser_2019-08-09_view.pdf)

[Higgins et al. *Considerations for implementing electronic laboratory notebooks in an academic research environment*, Nature Protocols, DOI: 10.1038/s41596-021-00645-8](https://doi.org/10.1038/s41596-021-00645-8)
