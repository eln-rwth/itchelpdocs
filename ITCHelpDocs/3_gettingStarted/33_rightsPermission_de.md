# Rollen- und Rechtekonzept in eLabFTW

Innerhalb des Benutzerkreises der eLabFTW-Instanz der RWTH gibt es verschiedene Rollen, die auf die im ELN gespeicherten Daten wie folgt zugreifen können:

| Rolle                                                                 | kann sich selber eines Teams und einer Rolle zuordnen | kann sich selber einer Gruppe und einer Rolle zuordnen |           kann eine Entity einsehen           |  kann eine Entity bearbeiten  |
| :-------------------------------------------------------------------- | :---------------------------------------------------: | :----------------------------------------------------: | :-------------------------------------------: | :---------------------------: |
| SysAdmin                                                              |                          ja                           |                ja <sup>1 </sup>                        |              ja <sup>1</sup>                  |              ja <sup>1</sup>  |
| Admin                                                                 |                         nein                          |                           ja                           |                    ja                         |              ja               |
| User - Entity-Owner                                                   |                         nein                          |                          nein                          |                    ja                         |              ja               |
| User - Team-Owner<br /> (kein Admin)                                  |                         nein                          |                          nein                          |         nein/ja <sup>2 </sup>                 | nein/ja <sup>2 </sup>         |
| User - nicht Entity-Owner                                             |                         nein                          |                          nein                          |         nein/ja <sup>2 </sup>                 | nein/ja <sup>2 </sup>         |
| Anonymer User<br /><br />(nur über SysAdmin-Einstellungen zulässig)   |                         nein                          |                          nein                          |         nein/ja <sup>2 </sup>                 | nein/ja <sup>2 </sup>         |

<sup>1</sup> muss sich die Rolle „Team-Admin“ zuordnen

<sup>2</sup> abhängig von der eingestellten Sichtbarkeit und Schreibrechten, definiert vom Entity-Owner

Die obige Tabelle basiert auf den SysAdmin-Einstellungen der Instanz der RWTH.