
# Roles and Permissions Concept in eLabFTW

Within the user circle of the RWTH's eLabFTW instance, there are various roles that can access the data stored in the ELN as follows:

| Role                                                                 | Can assign themselves to a team and role | Can assign themselves to a group and role |           Can view an entity            |  Can edit an entity   |
| :------------------------------------------------------------------- | :--------------------------------------: | :---------------------------------------: | :-------------------------------------: | :-------------------: |
| SysAdmin                                                             |                   yes                    |              yes <sup>1</sup>             |             yes <sup>1</sup>            |   yes <sup>1</sup>    |
| Admin                                                                |                   no                     |                   yes                     |                  yes                    |         yes           |
| User - Entity-Owner                                                  |                   no                     |                   no                      |                  yes                    |         yes           |
| User - Team-Owner<br /> (not Admin)                                  |                   no                     |                   no                      |      no/yes <sup>2</sup>                | no/yes <sup>2</sup>   |
| User - not Entity-Owner                                              |                   no                     |                   no                      |      no/yes <sup>2</sup>                | no/yes <sup>2</sup>   |
| Anonymous User<br /><br />(only allowed through SysAdmin settings)   |                   no                     |                   no                      |      no/yes <sup>2</sup>                | no/yes <sup>2</sup>   |

<sup>1</sup> must assign themselves the "Team-Admin" role

<sup>2</sup> dependent on the set visibility and editing rights, defined by the entity owner

The above table is based on the SysAdmin settings of RWTH's instance.