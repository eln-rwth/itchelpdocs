# Setting up your eLabFTW ELN

eLabFTW allows you to model a lot of your day-to-day lab work in a digital manner. It becomes especially usable if you setup templates for the various resources and experiments.

A good first step is to setup [**resources**](https://doc.elabftw.net/user-guide.html#resources). They can be sorted into color-coded categories. A good example of a resource would be a device, such as a microscope, which could also be designated as [bookable](https://doc.elabftw.net/user-guide.html#booking-resources). You can include images, instruction manuals, and so on when you set these up. It's best to setup some general templates for each category of resource so people adding information know what to include when adding a resource. Only Team Admins can [setup the categories](https://doc.elabftw.net/admin-guide.html#categories-tab), while anyone can create resources. Once created, these resources can be booked or linked to experiments or to one another. If you share devices with other teams in real life, this may be a good option within the ELN as well. You can make resources visible and editable by others.

Next, you should start creating [templates](https://doc.elabftw.net/user-guide.html#templates) for experiments. Especially when representing measurements, it's good to setup default templates and decide on associated metadata as a group. Templates can also be copied and expanded upon. The more you can unify the information captured, the more interoperable and transferable your research becomes, adding value for the group as a whole.

Once you feel you've got the basics down, start testing. This could involve a smaller user group at first. Setup regular review meeting to make updates and changes. Once your are ready to roll out the system for everyone, this group should give onboarding and training sessions.

Keep in mind: the system is flexible and is constantly being updated. If you are missing features, get in touch with the central ELN@RWTH team via the [IT-ServiceDesk](mailto:servicedesk@itc.rwth-aachen.de).
