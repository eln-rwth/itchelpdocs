# Anmeldung beim Dienst

Um sich bei der [Forschungsinstanz von eLabFTW an der RWTH](https://research.eln.rwth-aachen.de/) anzumelden, müssen Sie Ihre Institution aus der Dropdown-Liste auswählen. 

![eLabFTW Anmeldeseite](Screenshot_Login_eLabFTW_de.png.png)

Anschließend werden Sie zur lokalen Shibboleth-Single-Sign-On (SSO)-Seite weitergeleitet. Geben Sie Ihre Anmeldeinformationen ein und Sie gelangen zum ELN.



Wenn Sie die unten abgebildete Fehlermeldung sehen, wenden Sie sich entweder an Ihren Team-Administrator, um Ihr Benutzerkonto zu erstellen, oder [beantragen Sie ein neues Team](/ITCHelpDocs/2_newTeams/21_requestNewTeam_de.md) über den [IT-ServiceDesk](mailto:servicedesk@itc.rwth-aachen.de).

![Fehlermeldung bei der Anmeldung bei eLabFTW.](signin_error.png)