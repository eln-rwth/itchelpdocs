# Terminology (see the [documentation](https://doc.elabftw.net/generalities.html#lexicon))

**Instance**: A running eLabFTW service, for example: https://research.eln.rwth-aachen.de

**Team**: A team generally corresponds to a real research group or institute. A user can belong to multiple teams if needed and must select a team upon login. Each team has one or more administrators who can change many settings affecting users in the team, such as the default template for experiments, categories for database elements (element types), experiment status, tags, etc. Teams are created by the SysAdmin.

**SysAdmin**: A user with SysAdmin rights can change the instance configuration and create teams. Usually, this is the same person who installed the instance.

**Admin**: A user with Admin rights for a specific team has access to the Admin panel and can manage settings related to their team. A specific user can be an administrator in Team A and a user in Team B. There must be at least one Admin per team. Admins can appoint other Admins.

**Owner**: Owners are users who have created an entity.

**User**: A user with an account on the instance who belongs to at least one team.

**Entity**: An entity is an entry that can be an experiment, a resource, or an experiment template.
By default, experiments and resources are limited to a team. However, users can extend this to all registered users or even to anonymous users if the SysAdmin allows it.
Experiments belong to a user (owner), while resources are for the entire team and can be edited by any team member (with default permissions).
When users create or edit an entity, they cannot delete it but can only archive it. This corresponds to the handling of physical lab books.