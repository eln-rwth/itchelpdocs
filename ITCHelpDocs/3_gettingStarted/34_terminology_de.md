# Terminologie (vgl. die [Dokumentation](https://doc.elabftw.net/generalities.html#lexicon))

**Instanz**: Ein laufender eLabFTW-Dienst, zum Beispiel: https://research.eln.rwth-aachen.de

**Team**: Ein Team entspricht im Allgemeinen einer realen Forschungsgruppe oder einem Institut. Ein Benutzer kann bei Bedarf mehreren Teams angehören und muss sich beim Einloggen für ein Team entscheiden. Jedes Team hat einen oder mehrere Administratoren, die viele Einstellungen ändern können, die die Benutzer im Team betreffen, wie z. B. die Standardvorlage für Experimente, Kategorien für Datenbankelemente (Elementtypen), Experimentstatus, Tags usw. Teams werden vom SysAdmin erstellt.

**SysAdmin**: Eine Benutzer*in mit SysAdmin-Rechten kann die Instanzkonfiguration ändern und Teams erstellen. In der Regel ist es dieselbe Person, die die Instanz installiert hat.

**Admin**: Eine Benutzer*in mit Admin-Rechten für ein bestimmtes Team hat Zugang zum Admin-Panel und kann Einstellungen im Zusammenhang mit seinem Team verwalten. Ein bestimmter Benutzer kann in Team A Administrator und in Team B Benutzer sein. Es muss mindestens einen Admin pro Team geben. Admins können weitere Admins benennen.

**Owner**: Owner sind Benutzer*innen, die eine Entität erstellt haben.

**Benutzer**: Ein Benutzer*in mit einem Konto auf der Instanz, der mindestens einem Team angehört.

**Entity**: Eine Entity ist ein Eintrag, welches ein Experiment, eine Ressource oder eine Experimentvorlage sein kann.
Standardmäßig sind Experimente und Ressourcen auf ein Team beschränkt. Benutzer*innen können dies jedoch auf alle registrierten Benutzer oder sogar auf anonyme Benutzer ausdehnen, wenn der/die SysAdmin dies zulässt.
Experimente gehören einer Benutzer*in (Owner), während Ressourcen für das gesamte Team gelten und von jedem aus dem Team bearbeitet werden können (mit Standardberechtigungen).
Wenn Benutzer*innen eine Entity erstellen oder bearbeiten, können sie sie nicht löschen, sondern nur archivieren. Dies entspricht der Handhabung bei physischen Laborbüchern.
