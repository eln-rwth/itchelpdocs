# Teamrichtlinien, -standards und -richtlinien

Dieser Artikel betrifft nicht die Software selbst und zielt darauf ab, sicherzustellen, dass Ihr Institut oder Ihre Forschungsgruppe effektiv zu einem ELN übergeht.

Im Gegensatz zu einem Papierlaborbuch haben ELNs die Möglichkeit, Ihr Labor digital zu repräsentieren, anstatt die Forschung einer Person zu dokumentieren. Daher ist es wichtig, Nutzungstandards, Richtlinien und Leitlinien innerhalb Ihrer Gruppe festzulegen. Es wird empfohlen, eine dafür zuständige Arbeitsgruppe zu benennen, um den Service zu testen, innerhalb der Gruppe zu implementieren und zu pflegen sowie zu aktualisieren, wie die Gruppe mit dem Tool arbeitet.

Diese Arbeitsgruppe (oder zumindest ein Mitglied davon) dient auch als Hauptansprechpartner*in für die zentrale Unterstützung im IT Center und kann somit gewünschte Funktionen, Probleme usw. kommunizieren. Mindestens eine Person in dieser Gruppe muss auch ein [Team Admin](/ITCHelpDocs/2_newTeams/22_teamAdmin_de.md) sein, damit sie Standards, Einschränkungen und andere Maßnahmen im Tool selbst implementieren kann.

## Templates

Ressourcen- und Experimentvorlagen können die Dokumentation von Daten erheblich erleichtern und sicherstellen, dass alle Gruppenmitglieder ihre Arbeit auf ähnliche Weise dokumentieren.

Team Admins können die Verwendung von Vorlagen erzwingen. Einigen Sie sich auf Standards bzgl. welche Informationen gesammelt werden müssen. Dies kann eine Gruppenarbeit oder eine individuelle Anstrengung sein, aber Personen, die mit ähnlichen Methoden arbeiten, sollten sich gegenseitig beraten, um sicherzustellen, dass ihre Dokumentation in gewissem Maße konsistent ist. Darüber hinaus enthalten Ressourcen wie Geräte alle ähnliche Informationen wie Hersteller, Land, Herstellungsjahr und Modell. Die Aufnahme all dieser Informationen in eine Vorlage stellt sicher, dass keine Informationen versehentlich ausgelassen werden, wenn beispielsweise ein neues Gerät erworben und dem ELN hinzugefügt wird.

## Digitalisieren Sie das Labor

Beginnen Sie damit, alle Arbeitsabläufe Ihrer Laborforschungsdaten zu sammeln. Dies modelliert, wie Informationen in Ihrem Labor fließen und wie sie miteinander verbunden sind.

![Beispiel einer Mindmap zur Modellierung von Laborprozessen](2023-09-16_Data-Map_Example-1.png)

Beispiel zur Modellierung von Forschungsaktivitäten und -arbeitsabläufen. Bildnachweis: Grünwald, K.E., Poster: *Von einer Dateninventur zu einem Datenmanagementplan (DMP)*, DOI [10.18154](https://doi.org/10.18154/RWTH-2023-10436).

Es dient zwei Zwecken im Zusammenhang mit der Einrichtung und einem ELN:

1. Identifizieren Sie, was im ELN erfasst werden muss (z.B. Geräte, Software...)
2. Identifizieren Sie, welche Arbeitsabläufe verbessert oder automatisiert werden können (z.B. automatisches Anhängen von Rohdatendateien an Experimente)

## Einführung und Ausscheiden von Gruppenmitgliedern

Neue Gruppenmitglieder müssen wissen, wie von ihnen erwartet wird, das ELN zu nutzen. Welche Lese-/Schreibberechtigungen sollen Experimente haben? Wer darf was sehen? Welche Daten müssen den Experimenten angehängt werden?

Möglicherweise noch wichtiger ist es, zu entscheiden, wie Experimente am Ende des Arbeitsverhältnisses an Gruppenleiter übergeben werden sollen. Dies könnte die Übertragung des Owners, das Exportieren aller Experimente an eine Archivlösung oder einfach die Verwendung der Archivfunktion innerhalb des Notizbuchs beinhalten. **Es ist wichtig sicherzustellen, dass Vorgesetzte vollen Zugriff auf abgeschlossene Experimente haben, um auf die darin enthaltenen Informationen zugreifen, diese anzeigen und wiederverwenden zu können.**

## Weitere Ressourcen

[ZB MED ELN-Wegweiser](https://www.zbmed.de/fileadmin/user_upload/PUBLISSO/PUBLISSO_ELN-Wegweiser_2019-08-09_view.pdf)

[Higgins et al. *Considerations for implementing electronic laboratory notebooks in an academic research environment*, Nature Protocols, DOI: 10.1038/s41596-021-00645-8](https://doi.org/10.1038/s41596-021-00645-8)
