# TeamAdmin & SysAdmins: User Exit Strategies

Team members can easily be archived or their access limited by setting their valid until date in eLabfTW. However, when a user is part of multiple teams, the situation may be more complicated. The flowchart below aims to assist team admins in understanding their options:

![user exit strategy flowchart](eLabFTW_userExit_en.jpg)

1. **Archiving of Users and Entries:**  
   If a person is only a member of one team, they can be archived by a Team Admin. If desired, their associated entries can also be locked and archived. This method ensures a clean separation but is only suitable if the person exclusively belongs to one team. If the person is active in multiple teams, archiving them would also archive them in the other teams, which is generally not desired.

2. **Adjusting Rights for Visibility and Write Permissions:**  
   Another exit strategy involves explicitly setting the visibility and write permissions of entries for the team that is to be left. In order for the entries to remain visible to the team even after the person's departure, the corresponding rights for all entries must be adjusted. This can be done by an admin as a collective action through the admin control center. This way, access to experiments and other entries for the team remains ensured without the person still being part of the team.

3. **Transferring Ownership or Duplicating Entries**
   Another option is to transfer ownership of entries to another person in the team. Alternatively, relevant entries can be duplicated before removing the person from the team. Once a person has been removed from the team, their entries are no longer visible to that team, leading to uncertainty about who originally owned those entries. To avoid this, the name of the original creators can be included in the title of the entry or tagged behind it to make data origin more visible.

4. **Limiting Visibility of Entries**:  
   Instead of removing a person from the team, they can remain in it while their entry visibility is restricted. Through rights management, it can be determined that only the original owner and admins have access to specific entries. This prevents the individual leaving from accessing information that is not relevant to them while still remaining part of the team. Team Admins may also lock items, ensuring they are in read-only mode.

## General Aspects Considered by the Points Above

There are various methods to remove a user from a team: (1) archive, (2) delete, (3) removal from team by SysAdmin.

- Deleting is only possible when the user does not own any entries and it deletes them from all teams.
- Archiving a user archives them in all teams.
- Setting the _valid until_ date for a User sets this date for all teams they are a member of.
- Removing the User from the team makes their entries inaccessible to others (including (Sys)Admins) unless other Users, Teams, or Groups are explicitly added in the permission settings. This may be reversed by adding them back to the team.
