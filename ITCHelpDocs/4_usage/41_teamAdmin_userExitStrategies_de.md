# TeamAdmin: User Exit Strategies

Teammitglieder können einfach archiviert oder ihr Zugriff eingeschränkt werden, indem ihr Gültigkeitsdatum in eLabfTW festgelegt wird. Wenn eine Person jedoch Teil mehrerer Teams ist, kann die Situation komplizierter sein. Das folgende Flussdiagramm soll Team Admins dabei helfen, die Optionen zu verstehen:

![user exit strategy flowchart](eLabFTW_userExit_de.jpg)

1. **Archivierung der Nutzenden und der Einträge**  
   Wenn eine Person nur Mitglied in einem Team ist, kann diese vom Team Admin archiviert werden. Dabei lässt sich entscheiden, ob die zugehörigen Einträge ebenfalls gesperrt und archiviert werden sollen. Diese Methode sorgt für eine saubere Trennung, ist jedoch nur geeignet, wenn die Person ausschließlich einem Team angehört. Falls die Person in mehreren Teams aktiv ist, würde sie durch die Archivierung auch in den anderen Teams archiviert werden, was in der Regel nicht gewünscht ist.

2. **Anpassung der Rechte für Sichtbarkeit und Schreibrechte**  
   Eine weitere Exit-Strategie besteht darin, die Sichtbarkeit und die Schreibrechte der Einträge explizit für das Team, das verlassen werden soll, festzulegen. Damit die Einträge auch nach dem Austritt der Person weiterhin für das Team sichtbar bleiben, müssen die entsprechenden Rechte für alle Einträge angepasst werden. Dies kann durch eine*n Team-Admin als Sammelaktion über das Admin-Kontrollzentrum erfolgen. Auf diese Weise bleibt der Zugriff auf die Experimente und Einträge für das Team gewährleistet, ohne dass die Person selbst noch Teil des Teams ist. Die bzw. der SysAdmin kann die Person von dem jeweiligem Team entfernen.

3. **Übertragung der Eigentümerschaft oder Duplizieren der Einträge**  
   Eine weitere Möglichkeit besteht darin, die Eigentümerschaft der Einträge auf eine andere Person im Team zu übertragen. Alternativ können relevante Einträge dupliziert werden, bevor die Person aus dem Team entfernt wird. Sobald eine Person aus dem Team entfernt wurde, sind deren Einträge für das Team nicht mehr sichtbar, was dazu führt, dass nicht mehr ersichtlich ist, wem die Einträge ursprünglich gehörten. Um dies zu vermeiden, kann der Name der ursprünglichen Ersteller*innen im Titel des Eintrags oder als Tag hinterlegt werden, um die Herkunft der Daten besser sichtbar zu machen.
   Die bzw. der SysAdmin kann die Person von dem jeweiligem Team entfernen.
   
4. **Einschränkung der Sichtbarkeit der Einträge**  
   Statt die Person aus dem Team zu entfernen, kann sie im Team belassen werden, während die Sichtbarkeit ihrer Einträge eingeschränkt wird. Über die Rechteverwaltung lässt sich festlegen, dass nur die ursprünglichen Eigentümer*in und Admins Zugriff auf bestimmte Einträge haben. So wird verhindert, dass die Person, die das Team verlassen soll, auf für sie nicht relevante Informationen zugreift, während sie weiterhin im Team verbleibt. Team Admins können Einträge zusätzlich sperren. Somit sind sie schreibgeschützt. 

## Allgemeine Aspekte, die in den obigen Punkten berücksichtigt werden

Es gibt verschiedene Methoden, um einen Benutzer aus einem Team zu entfernen: (1) archivieren, (2) löschen, (3) Entfernung aus dem Team durch den SysAdmin.

- Das Löschen ist nur möglich, wenn der User keine Einträge besitzt und es löscht die Person aus allen Teams.
- Das Archivieren eines Users archiviert die Person in allen Teams.
- Die Entfernung des Users aus dem Team macht die Einträge der Person für andere unzugänglich (inkl. (Sys)Admins), es sei denn, andere User, Teams oder Gruppen werden ausdrücklich in den Berechtigungseinstellungen hinzugefügt. Die kann rückgängig gemacht werden in dem die Person dem Team wieder hinzugefügt wird.
