# Gemeinschaft und Ressourcen

## Chat

Das zentrale ELN@RWTH hat einen zentralen Matrix-Chatraum für eine einfache Kommunikation eingerichtet.
Dies ist für allgemeine Themen im Zusammenhang mit ELNs gedacht und ist *nicht* auf eLabFTW oder die zentrale Instanz der RWTH beschränkt.
Derzeit handelt es sich nicht um einen öffentlichen Chatroom, was sich in Zukunft ändern könnte.
Für den Zugang senden Sie eine E-Mail an den [IT-ServiceDesk](mailto:servicedesk@itc.rwth-aachen.de).

Es gibt auch einen zentralen [Chatraum für eLabFTW](https://app.gitter.im/#/room/#elabftw_elabftw:gitter.im) für schnelle Hilfe von den Entwicklern und der internationalen eLabFTW-Community.

## GitLab-Gruppe

Die [ELN-RWTH GitLab-Gruppe](https://git.rwth-aachen.de/eln-rwth) dient als zentraler Ort zur Sammlung von Material und Informationen über ELNs. Dies ist eine laufende Gemeinschaftsarbeit – fühlen Sie sich frei, beizutragen!