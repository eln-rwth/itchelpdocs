---
title: "Allgemeine Nutzungsbedingungen für den Dienst ELN@RWTH (Pilotphase)"
author: Nicole Parks, Lukas C. Bossert / IT Center
date: "2024-05-24"
keywords: ELN@RWTH
---

## Präambel
Der Dienst „ELN@RWTH“ stellt ein webbasiertes elektronisches Labornotizbuch-System dar, 
das Forschenden die Aufzeichnung und Verarbeitung von Daten gemäß der GLP (Good
Laboratory Practice)-konformer Weise erleichtert.
Das Dienst wird immer Sinne eines Software-as-a-Service (SaaS) für die RWTH Aachen University betrieben und durch das IT Center unterstützt.

## Nutzung des Dienstes „ELN@RWTH“

Alle Angehörigen der RWTH Aachen University sind berechtigt den Dienst „ELN@RWTH“ zur Unterstützung ihrer Forschungstätigkeiten mit der Instanz „research.eln.rwth-aachen.de“ sowie in der Lehre mit der Instanz „education.eln.rwth-aachen.de“ zu nutzen.

Die hier aufgeführten Nutzungsbedingungen müssen von den Nutzenden vor der Nutzung des Services beim ersten Login in die jeweilige Instanz akzeptiert werden. Nachdem die Nutzungsbedingungen einmal bestätigt wurden, ist keine weitere Bestätigung notwendig, sofern die Nutzungsbedingungen sich nicht ändern. Möchte ein Nutzender die Nutzungsbedingungen erneut abrufen, werden die aktuellen Nutzungsbedingungen in unserem Dokumentationsportal veröffentlicht (itc.help).

Bei Verstoß gegen diese Nutzungsbedingungen behalten wir uns die Sperrung einzelner Projekte und Accounts vor.

### Kosten

Die Nutzung des Dienstes „ELN@RWTH“ ist kostenlos.

## Datenspeicherung und -löschung

### Art der gespeicherten Daten

Die im elektronischen Labornotizbuch (ELN) gespeicherten Daten enthalten Forschungsdaten, z.B. Messdaten, Interviewdaten, Dokumentation und Metadaten, die im Sinne eines Forschungsunternehmens erhoben wurden. Hierzu gehören gewisse Personenbezogene Daten, wie beispielsweise der Name und die Kontaktdaten eines Forschenden. Diese dienen ausschließlich der Dokumentation der Forschungstätigkeiten (vgl. „Leitlinien zur Sicherstellung guter wissenschaftlicher Praxis“, DFG, August 2019).

Die Verarbeitung, Speicherung oder Archivierung sensibler Forschungsdaten (vgl. Art. 9 DSGVO) im ELN ist ausdrücklich untersagt. Die Verwendung des ELN hat
unter strikter Einhaltung der Erfordernisse Guter Wissenschaftlicher Praxis zu
geschehen.

#### Löschung der Daten

Für die Einhaltung der Dokumentationspflicht gemäß der Guten
Wissenschaftlichen Praxis werden Nutzendenkonten und damit die
erstellten Einträge innerhalb des ELN für mindestens 10 Jahre vorgehalten.

* Stammdaten: Nutzendendaten können von Administratoren archiviert werden. Somit
  sind alle Experimente des Nutzenden gesperrt sowie deren Login zum System
  deaktiviert. Zudem können diese personenbezogenen Daten auf Anfrage über die
  SysAdmins anonymisiert werden. Das vollständige Löschen von Daten ist
  innerhalb des ELN nicht vorgesehen.
* Metadaten und Forschungsdaten (wenn personenbezogen): Für die Einhaltung der
  Dokumentationspflicht gemäß der Guten Wissenschaftlichen Praxis werden
  Nutzendenkonten und damit die erstellten Einträge innerhalb des ELN für
  mindestens 10 Jahre vorgehalten. Sie können nach aktuellem Stand nicht
  gelöscht werden.

#### Datenschutz

Die Erhebung, Nutzung und Verwendung personenbezogener Daten erfolgt unter Beachtung der einschlägigen Datenschutzbestimmungen. Insbesondere werden keine personenbezogenen Daten unbefugt an Dritte weitergegeben. Personenbezogene Daten werden zu Forschungszwecken und zur Nachvollziehbarkeit der Forschzungsergebnisse sowie zum technischen Betrieb des Dienstes genutzt.

## Rechte und Pflichten des Nutzenden

Die Nutzenden sind ausschließlich für die im Dienst „ELN@RWTH“ eingestellten Daten, deren Qualität und deren Korrektheit, sowie die Beachtung von Rechten Dritter verantwortlich. Hierbei ist auch auf die Einhaltung der oben genannten [Art der zu speichernden Daten](##art-der-gespeicherten-daten) zu achten.


#### Team Admins

Team Admins stellen eine gesonderte Nutzendengruppe dar. Sie verwalten ihr lokales Team. Ein Team kann beispielsweise ein Institut oder eine Arbeitsgruppe sein. Es kann mehrere Admins geben. Mindestens ein Admin ist Kontaktperson für die zentralen Systemadmins am IT Center.

#### Zugangsdaten

Nutzende sind verpflichtet ihre Zugangsdaten vertraulich zu behandeln und vor Zugriff durch Dritte zu schützen. Im Falle eines möglichen Missbrauchs von Zugangsdaten ist der lokale Administrator / die lokale Administratorin unverzüglich zu informieren. Nutzende sind für die Folgen eines Missbrauchs ihrer Zugangsdaten verantwortlich.
